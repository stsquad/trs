Build and install
#################
.. toctree::
    :maxdepth: 2

    build_source
    install_firmware
    update_firmware
