..
 # Copyright (c) 2023, Linaro Limited.
 #
 # SPDX-License-Identifier: MIT

#########################
Changelog & Release Notes
#########################

v0.3 - 2023-04-19
~~~~~~~~~~~~~~~~~
  * Xen support
     * It's now possible make a TRS build with Xen support. For more
       information about it, check the :ref:`Xen` documentation page.
  * Parsec
     * Will now automatically be able to use OP-TEE PKCS#11 and fTPM services.
  * SDK
     * EWAOL distro feature and OpenEmbedded SDK package has been enabled.

v0.2 - 2023-03-07
~~~~~~~~~~~~~~~~~
  * Stable CI
     * xtest from OP-TEE (nightly and merge request)
     * Measured boot tests (nightly and merge request)
     * Secure boot (nightly and merge request)
     * ACS 1.0 manually, except QEMU, where it is in CI.
  * Platform support, meaning that they work with TRS
     * QEMU
     * RockPi4
     * Synquacer
  * New features
     * Authenticated policies.
     * Grub as part of the boot flow.

v0.1 - 2022-12-16
~~~~~~~~~~~~~~~~~
* Restructured the layer structure, by moving some layers up to the top level.
* QEMU is built by Yocto instead of relying on the host installed QEMU version.
* Changed repo release/branching strategy.
* Trusted Substrate documentation has moved into a subsection of TRS.
* Uses Trusted Substrate v0.2.
* Uses LEDGE Secure v0.1.
* Features enabled: LUKS disc encryption, Measured Boot, UEFI Secure Boot using
  U-boot.

v0.1-beta - 2022-09-02
~~~~~~~~~~~~~~~~~~~~~~
.. note::

    This release is slightly flawed, mostly due to the fact that code was
    checked out when the build was started and the code did not always track
    stable commits.

* Builds TRS for the QEMU target.
* Boot cleanly up to the login prompt.
* Nothing tested.
* RockPi4 works, but not officially part of the v0.1-beta release.
